"use strict";

const DATA = [
  {
    "first name": "Олексій",
    "last name": "Петров",
    photo: "./img/trainers/trainer-m1.jpg",
    specialization: "Басейн",
    category: "майстер",
    experience: "8 років",
    description:
      "Олексій має багаторічний досвід роботи з плавцями. Він займається якісною підготовкою спортсменів на міжнародних змаганнях. Його методика базується на новітніх технологіях тренувань.",
  },
  {
    "first name": "Марина",
    "last name": "Іванова",
    photo: "./img/trainers/trainer-f1.png",
    specialization: "Тренажерний зал",
    category: "спеціаліст",
    experience: "2 роки",
    description:
      "Марина спеціалізується на роботі з ваговими тренажерами. Вона розробила унікальну програму для набору м'язової маси. Її клієнти завжди задоволені результатами.",
  },
  {
    "first name": "Ігор",
    "last name": "Сидоренко",
    photo: "./img/trainers/trainer-m2.jpg",
    specialization: "Дитячий клуб",
    category: "інструктор",
    experience: "1 рік",
    description:
      "Ігор працює з дітьми різного віку. Він створив ігрові методики для розвитку координації та спритності. Його уроки завжди цікаві та корисні для малюків.",
  },
  {
    "first name": "Тетяна",
    "last name": "Мороз",
    photo: "./img/trainers/trainer-f2.jpg",
    specialization: "Бійцівський клуб",
    category: "майстер",
    experience: "10 років",
    description:
      "Тетяна є експертом в бойових мистецтвах. Вона проводить тренування для професіоналів і новачків. Її підхід до навчання допомагає спортсменам досягати високих результатів.",
  },
  {
    "first name": "Сергій",
    "last name": "Коваленко",
    photo: "./img/trainers/trainer-m3.jpg",
    specialization: "Тренажерний зал",
    category: "інструктор",
    experience: "1 рік",
    description:
      "Сергій фокусується на роботі з фітнесом та кардіотренуваннями. Він вдосконалив свої методики протягом багатьох років. Його учні завжди в формі та енергійні.",
  },
  {
    "first name": "Олена",
    "last name": "Лисенко",
    photo: "./img/trainers/trainer-f3.jpg",
    specialization: "Басейн",
    category: "спеціаліст",
    experience: "4 роки",
    description:
      "Олена спеціалізується на синхронному плаванні. Вона тренує команди різного рівня. Її команди завжди займають призові місця на змаганнях.",
  },
  {
    "first name": "Андрій",
    "last name": "Волков",
    photo: "./img/trainers/trainer-m4.jpg",
    specialization: "Бійцівський клуб",
    category: "інструктор",
    experience: "1 рік",
    description:
      "Андрій має досвід у вивченні різних бойових мистецтв. Він викладає техніку та тактику бою. Його учні здобувають перемоги на міжнародних турнірах.",
  },
  {
    "first name": "Наталія",
    "last name": "Романенко",
    photo: "./img/trainers/trainer-f4.jpg",
    specialization: "Дитячий клуб",
    category: "спеціаліст",
    experience: "3 роки",
    description:
      "Наталія розробила унікальну програму для дітей дошкільного віку. Вона допомагає дітям розвивати фізичні та ментальні навички. Її класи завжди веселі та динамічні.",
  },
  {
    "first name": "Віталій",
    "last name": "Козлов",
    photo: "./img/trainers/trainer-m5.jpg",
    specialization: "Тренажерний зал",
    category: "майстер",
    experience: "10 років",
    description:
      "Віталій спеціалізується на функціональному тренуванні. Він розробив ряд ефективних тренувальних програм. Його клієнти швидко досягають бажаних результатів.",
  },
  {
    "first name": "Юлія",
    "last name": "Кравченко",
    photo: "./img/trainers/trainer-f5.jpg",
    specialization: "Басейн",
    category: "спеціаліст",
    experience: "4 роки",
    description:
      "Юлія є експертом у водних видах спорту. Вона проводить тренування з аквагімнастики та аеробіки. Її учні демонструють вражаючі показники на змаганнях.",
  },
  {
    "first name": "Олег",
    "last name": "Мельник",
    photo: "./img/trainers/trainer-m6.jpg",
    specialization: "Бійцівський клуб",
    category: "майстер",
    experience: "12 років",
    description:
      "Олег є визнаним майстром в бойових мистецтвах. Він тренує чемпіонів різних вагових категорій. Його методики вважаються одними з найефективніших у світі бойових мистецтв.",
  },
  {
    "first name": "Лідія",
    "last name": "Попова",
    photo: "./img/trainers/trainer-f6.jpg",
    specialization: "Дитячий клуб",
    category: "інструктор",
    experience: "1 рік",
    description:
      "Лідія має великий досвід у роботі з дітьми. Вона організовує різноманітні спортивні ігри та заняття. Її класи завжди допомагають дітям розвивати соціальні навички та командний дух.",
  },
  {
    "first name": "Роман",
    "last name": "Семенов",
    photo: "./img/trainers/trainer-m7.jpg",
    specialization: "Тренажерний зал",
    category: "спеціаліст",
    experience: "2 роки",
    description:
      "Роман є експертом у кросфіту та функціональних тренуваннях. Він розробив власні програми для різних вікових груп. Його учні часто отримують нагороди на фітнес-турнірах.",
  },
  {
    "first name": "Анастасія",
    "last name": "Гончарова",
    photo: "./img/trainers/trainer-f7.jpg",
    specialization: "Басейн",
    category: "інструктор",
    experience: "1 рік",
    description:
      "Анастасія фокусується на водних програмах для здоров'я та фітнесу. Вона проводить тренування для осіб з різним рівнем підготовки. Її учні відзначають покращення здоров'я та благополуччя після занять.",
  },
  {
    "first name": "Валентин",
    "last name": "Ткаченко",
    photo: "./img/trainers/trainer-m8.jpg",
    specialization: "Бійцівський клуб",
    category: "спеціаліст",
    experience: "2 роки",
    description:
      "Валентин є експертом в таеквондо та кікбоксингу. Він викладає техніку, тактику та стратегію бою. Його учні часто стають чемпіонами на національних та міжнародних змаганнях.",
  },
  {
    "first name": "Лариса",
    "last name": "Петренко",
    photo: "./img/trainers/trainer-f8.jpg",
    specialization: "Дитячий клуб",
    category: "майстер",
    experience: "7 років",
    description:
      "Лариса розробила комплексну програму для розвитку фізичних та інтелектуальних навичок дітей. Вона проводить заняття в ігровій формі. Її методика допомагає дітям стати активними та розумними.",
  },
  {
    "first name": "Олексій",
    "last name": "Петров",
    photo: "./img/trainers/trainer-m9.jpg",
    specialization: "Басейн",
    category: "майстер",
    experience: "11 років",
    description:
      "Олексій має багаторічний досвід роботи з плавцями. Він займається якісною підготовкою спортсменів на міжнародних змаганнях. Його методика базується на новітніх технологіях тренувань.",
  },
  {
    "first name": "Марина",
    "last name": "Іванова",
    photo: "./img/trainers/trainer-f9.jpg",
    specialization: "Тренажерний зал",
    category: "спеціаліст",
    experience: "2 роки",
    description:
      "Марина спеціалізується на роботі з ваговими тренажерами. Вона розробила унікальну програму для набору м'язової маси. Її клієнти завжди задоволені результатами.",
  },
  {
    "first name": "Ігор",
    "last name": "Сидоренко",
    photo: "./img/trainers/trainer-m10.jpg",
    specialization: "Дитячий клуб",
    category: "інструктор",
    experience: "1 рік",
    description:
      "Ігор працює з дітьми різного віку. Він створив ігрові методики для розвитку координації та спритності. Його уроки завжди цікаві та корисні для малюків.",
  },
  {
    "first name": "Наталія",
    "last name": "Бондаренко",
    photo: "./img/trainers/trainer-f10.jpg",
    specialization: "Бійцівський клуб",
    category: "майстер",
    experience: "8 років",
    description:
      "Наталія є майстром у бойових мистецтвах. Вона вивчала різні техніки та стили, із якими працює зі своїми учнями. Її підхід до навчання відповідає найвищим стандартам.",
  },
  {
    "first name": "Андрій",
    "last name": "Семенов",
    photo: "./img/trainers/trainer-m11.jpg",
    specialization: "Тренажерний зал",
    category: "інструктор",
    experience: "1 рік",
    description:
      "Андрій спеціалізується на функціональному тренуванні. Він розробив власну систему вправ для зміцнення корпусу. Його учні завжди отримують видимі результати.",
  },
  {
    "first name": "Софія",
    "last name": "Мельник",
    photo: "./img/trainers/trainer-f11.jpg",
    specialization: "Басейн",
    category: "спеціаліст",
    experience: "6 років",
    description:
      "Софія працює з аквагімнастикою. Вона вивчила різні техніки та стили плавання. Її заняття допомагають клієнтам розслабитися та покращити фізичну форму.",
  },
  {
    "first name": "Дмитро",
    "last name": "Ковальчук",
    photo: "./img/trainers/trainer-m12.png",
    specialization: "Дитячий клуб",
    category: "майстер",
    experience: "10 років",
    description:
      "Дмитро спеціалізується на розвитку дитячого спорту. Він розробив унікальну програму для малюків. Його методики забезпечують гармонійний розвиток дітей.",
  },
  {
    "first name": "Олена",
    "last name": "Ткаченко",
    photo: "./img/trainers/trainer-f12.jpg",
    specialization: "Бійцівський клуб",
    category: "спеціаліст",
    experience: "5 років",
    description:
      "Олена є відомим тренером у жіночому бойовому клубі. Вона вивчила різні техніки самооборони. Її підхід дозволяє її ученицям відчувати себе впевнено в будь-яких ситуаціях.",
  },
];

document.querySelector(".sorting").removeAttribute("hidden");
document.querySelector(".sidebar").removeAttribute("hidden");

document.addEventListener("DOMContentLoaded", function () {
  const trainerCardTemplate = document.getElementById("trainer-card");
  const trainersContainer = document.querySelector(
    ".trainers-cards__container"
  );
  const modalTemplate = document.getElementById("modal-template");
  const sortingButtons = document.querySelectorAll(".sorting__btn");

  function fillModalData(trainerData, modalContent) {
    const {
      photo,
      "first name": firstName,
      "last name": lastName,
      category,
      experience,
      specialization,
      description,
    } = trainerData;

    modalContent.querySelector(".modal__img").src = photo;
    modalContent.querySelector(
      ".modal__name"
    ).textContent = `${firstName} ${lastName}`;
    modalContent.querySelector(
      ".modal__point--category"
    ).textContent = `Категорія: ${category}`;
    modalContent.querySelector(
      ".modal__point--experience"
    ).textContent = `Досвід: ${experience} років`;
    modalContent.querySelector(
      ".modal__point--specialization"
    ).textContent = `Напрям тренера: ${specialization}`;
    modalContent.querySelector(".modal__text").textContent = description;
  }

  function renderTrainers(data) {
    trainersContainer.innerHTML = "";

    data.forEach(function (trainerData) {
      let trainerCard = document.importNode(trainerCardTemplate.content, true);
      const {
        photo,
        "first name": firstName,
        "last name": lastName,
      } = trainerData;

      trainerCard.querySelector(".trainer__img").src = photo;
      trainerCard.querySelector(
        ".trainer__name"
      ).textContent = `${firstName} ${lastName}`;

      trainerCard
        .querySelector(".trainer__show-more")
        .addEventListener("click", function () {
          const cloneModal = document.importNode(modalTemplate.content, true);
          fillModalData(trainerData, cloneModal);
          document.body.appendChild(cloneModal);
          document.body.style.overflow = "hidden";
        });

      trainersContainer.appendChild(trainerCard);
    });
  }

  renderTrainers(DATA);

  function sortTrainers(sortKey, sortOrder) {
    const sortedData = [...DATA];

    sortedData.sort((a, b) => {
      if (sortKey === "last name") {
        return sortOrder === "asc"
          ? a["last name"].localeCompare(b["last name"])
          : b["last name"].localeCompare(a["last name"]);
      } else if (sortKey === "experience") {
        const experienceA = parseInt(a.experience);
        const experienceB = parseInt(b.experience);
        return sortOrder === "asc"
          ? experienceB - experienceA
          : experienceA - experienceB;
      }
      return 0;
    });

    renderTrainers(sortedData);
  }

  function removeActiveClass() {
    document.querySelectorAll(".sorting__btn").forEach(function (btn) {
      btn.classList.remove("sorting__btn--active");
    });
  }

  function addSortingAttributes(button, sort, order) {
    button.setAttribute("data-sort", sort);
    button.setAttribute("data-order", order);
  }

  addSortingAttributes(sortingButtons[0], "default", "asc");
  addSortingAttributes(sortingButtons[1], "last name", "asc");
  addSortingAttributes(sortingButtons[2], "experience", "asc");

  function handleSorting(button) {
    removeActiveClass();

    button.classList.add("sorting__btn--active");
    const sortKey = button.getAttribute("data-sort");
    const sortOrder = button.getAttribute("data-order");

    sortTrainers(sortKey, sortOrder);
  }

  document.querySelectorAll(".sorting__btn").forEach(function (button) {
    button.addEventListener("click", function () {
      handleSorting(button);
    });
  });

  document.body.addEventListener("click", (event) => {
    const closeButton = event.target.closest(".modal__close");
    if (closeButton) {
      const cloneModal = closeButton.closest(".modal");
      if (cloneModal && document.body.contains(cloneModal)) {
        document.body.removeChild(cloneModal);
        document.body.style.overflow = "";
      }
    }
  });

  const submitButton = document.querySelector(".filters__submit");
  const directionRadios = document.querySelectorAll('input[name="direction"]');
  const categoryRadios = document.querySelectorAll('input[name="category"]');

  submitButton.addEventListener("click", function (event) {
    event.preventDefault();

    const selectedDirection =
      Array.from(directionRadios).find((input) => input.checked)?.value ||
      "all";
    const selectedCategory =
      Array.from(categoryRadios).find((input) => input.checked)?.value || "all";

    const directionMapping = {
      "swimming pool": "Басейн",
      gym: "Тренажерний зал",
      "kids club": "Дитячий клуб",
      "fight club": "Бійцівський клуб",
    };

    const categoryMapping = {
      specialist: "Спеціаліст",
      instructor: "Інструктор",
      master: "Майстер",
    };

    const filteredTrainers = DATA.filter((item) => {
      const translatedDirection =
        directionMapping[selectedDirection.toLowerCase()];
      const translatedCategory =
        categoryMapping[selectedCategory.toLowerCase()];

      const matchDirection =
        selectedDirection.toLowerCase() === "all" ||
        item.specialization.toLowerCase() === translatedDirection.toLowerCase();
      const matchCategory =
        selectedCategory.toLowerCase() === "all" ||
        item.category.toLowerCase() === translatedCategory.toLowerCase();

      return matchDirection && matchCategory;
    });

    localStorage.setItem("filteredTrainers", JSON.stringify(filteredTrainers));

    const sortKey =
      document
        .querySelector(".sorting__btn--active")
        ?.getAttribute("data-sort") || "default";
    const sortOrder =
      document
        .querySelector(".sorting__btn--active")
        ?.getAttribute("data-order") || "asc";
    sortTrainers(sortKey, sortOrder, filteredTrainers);
    renderTrainers(filteredTrainers);
  });
});
